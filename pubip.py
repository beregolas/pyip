import requests

if __name__ == "__main__":
    r_dns = requests.get("http://ip-api.com/json/")
    result = r_dns.json()

    if "status" in result and result["status"] == "success":
        print("You public facing IP is: {} \nYou appear to be located in {}, {}.\nYour ISP has been recognized as \"{}\""
              .format(result["query"], result["city"], result["country"], result["isp"])
              )
    else:
        print("The request failed. Check your connection and try again")

